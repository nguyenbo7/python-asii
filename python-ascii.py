#   A dictionary contain all alphabet character
#       an alphabet as a key
#       an ascii as a value
dict_ascii_value = {
    "A": 65,    "G": 71,    "M": 77,    "S": 83,    "Y": 89,
    "B": 66,    "H": 72,    "N": 78,    "T": 84,    "Z": 90,
    "C": 67,    "I": 73,    "O": 79,    "U": 85,
    "D": 68,    "J": 74,    "P": 80,    "V": 86,
    "E": 69,    "K": 75,    "Q": 81,    "W": 87,
    "F": 70,    "L": 76,    "R": 82,    "X": 88,

    "a": 97,     "g": 103,    "m": 109,    "s": 115,    "y": 121,
    "b": 98,     "h": 104,    "n": 110,    "t": 116,    "z": 122,
    "c": 99,     "i": 105,    "o": 111,    "u": 117,
    "d": 100,    "j": 106,    "p": 112,    "v": 118,
    "e": 101,    "k": 107,    "q": 113,    "w": 119,
    "f": 102,    "l": 108,    "r": 114,    "x": 120,
}

#   Helper function
#   function to return the key from the value in a dictionary


def get_key(dict, val):
    for key, value in dict.items():
        if val == value:
            return key


#   Question 1:----------------------------------------
#   function to return the uppercase version of a letter
#   check each letter if it is lowercase (in range of 97 and 122)
#   then subtract its index 32 to find the corresponding value of uppercase
def to_uppercase(text_param):
    result = ""
    for x in text_param:

        if x in dict_ascii_value:
            char_index = dict_ascii_value[x]

            if char_index >= 97 and char_index <= 122:
                upper_char_index = char_index - 32
                result = result + get_key(dict_ascii_value, upper_char_index)
            else:
                result = result + x
        else:
            result = result + x
    return result


#   Uncomment to test the function
# print("Test 1: Take an empty string:\n")
# print(to_uppercase(""))

# print("Test 2: Take a string with all Uppercase:\n")
# print(to_uppercase("QWERTYUIOPASDFGHJKLZXCVBNM"))
# print("\n")

# print("Test 3: Take a string with all Lowercase:\n")
# print(to_uppercase("qwertyuiopasdfghjklzxcvbnm"))
# print("\n")

# print("Test 4: Take a string with all number:\n")
# print(to_uppercase("123456789"))
# print("\n")

# print("Test 5: Take a string with special character:\n")
# print(to_uppercase("!@#$%^&*&(()   ?><}{|:"))
# print("\n")

# print("Test 6: Take a string with mixed:\n")
# print(to_uppercase("Hello    World!  Haloo ^__^"))
# print("\n")


#   Question 2:----------------------------------------
#   function to return the lowercase version of a letter
#   check each letter if it is lowercase (in range of 65 and 90)
#   then add its index 32 to find the corresponding value of lowercase
def to_lowercase(text_param):
    result = ""
    for x in text_param:

        if x in dict_ascii_value:
            char_index = dict_ascii_value[x]

            if char_index >= 65 and char_index <= 90:
                lower_char_index = char_index + 32
                result = result + get_key(dict_ascii_value, lower_char_index)
            else:
                result = result + x
        else:
            result = result + x
    return result


#   Uncomment to test the function
# print("Test 1: Take an empty string:\n")
# print(to_lowercase(""))

# print("Test 2: Take a string with all Uppercase:\n")
# print(to_lowercase("QWERTYUIOPASDFGHJKLZXCVBNM"))
# print("\n")

# print("Test 3: Take a string with all Lowercase:\n")
# print(to_lowercase("qwertyuiopasdfghjklzxcvbnm"))
# print("\n")

# print("Test 4: Take a string with all number:\n")
# print(to_lowercase("123456789"))
# print("\n")

# print("Test 5: Take a string with special character:\n")
# print(to_lowercase("!@#$%^&*&(()   ?><}{|:"))
# print("\n")

# print("Test 6: Take a string with mixed:\n")
# print(to_lowercase("Hello    World!  Haloo ^__^"))
# print("\n")


#   Question 3:----------------------------------------
#   function to return true if the letter is an alphabet
#   if the letter exists in the dictionary of ascii then true
#   assume take only one character
def is_an_alphabet(letter_param):
    if letter_param in dict_ascii_value:
        return True
    return False

#   Uncomment to test the function
# print("Test 1: Take an empty string:\n")
# print(is_an_alphabet(""))
# print("\n")

# print("Test 2: Take a digit:\n")
# print(is_an_alphabet("1"))
# print("\n")

# print("Test 3: Take an alphabet:\n")
# print(is_an_alphabet("a"))
# print("\n")

# print("Test 4: Take an special character:\n")
# print(is_an_alphabet("$"))
# print("\n")


#   Question 4:----------------------------------------
#  function to return true if the element is a digit
#   if the element exists in the array number then true
#   assume take only one character
def is_a_digit(element):
    array_num = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    array_num_string = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
    if element in array_num or element in array_num_string:
        return True
    return False

#   Uncomment to test the function
# print("Test 1: Take an empty string:\n")
# print(is_a_digit(""))
# print("\n")

# print("Test 2: Take a digit:\n")
# print(is_a_digit("1"))
# print("\n")

# print("Test 3: Take an alphabet:\n")
# print(is_a_digit("a"))
# print("\n")

# print("Test 4: Take an special character:\n")
# print(is_a_digit("$"))
# print("\n")


#   Question 5:----------------------------------------
#   function to return true if the character is special
#   if a character is neither an alphabet or a digit then true
#   assume take only one character
def is_special_char(character):
    if is_an_alphabet(character) or is_a_digit(character):
        return False
    return True


#   Uncomment to test the function
# print("Test 1: Take an empty string:\n")
# print(is_special_char(""))
# print("\n")

# print("Test 2: Take a digit:\n")
# print(is_special_char("1"))
# print("\n")

# print("Test 3: Take an alphabet:\n")
# print(is_special_char("a"))
# print("\n")

# print("Test 4: Take an special character:\n")
# print(is_special_char("$"))
# print("\n")
